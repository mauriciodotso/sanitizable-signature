from __future__ import division
import random
import hashlib
import fractions
from decimal import *


class CHash(object):
    def digest(self, m, j, tag, r, pk):
        """
        Produces a chameleon hash based on RSA keys of the following work:
        Ateniese, Giuseppe, and Breno De Medeiros. "On the key exposure problem
        in chameleon hashes."International Conference on Security in
        Communication Networks. Springer Berlin Heidelberg, 2004.
        """
        #The concatenation of tag
        digest = hashlib.sha1(str(tag) + "|" + m).hexdigest()
        #print("digest: {}".format(digest))
        h = int(digest, 16)
        #print("h: {}".format(h))
        #print("J: {}".format(J))

        ch = (pow(j, h, pk[1]) * pow(r, pk[0], pk[1])) % pk[1]
        return ch

    def egcd(self, a, b):
        if a == 0:
            return (b, 0, 1)
        else:
            g, y, x = self.egcd(b % a, a)
            return (g, x - (b // a) * y, y)

    def modinv(self, a, m):
        g, x, y = self.egcd(a, m)
        if g != 1:
            raise Exception('modular inverse does not exist')
        else:
            return x % m


    def adapt(self, m, j, tag, r, m2, tag2, keypair):
        """
        Collides the chameleon hash based on RSA keys
        """
        m_digest = hashlib.sha1(str(tag) + "|" + m).hexdigest()
        m2_digest = hashlib.sha1(str(tag2) + "|" + m2).hexdigest()

        h_m = int(m_digest, 16)
        h_m2 = int(m2_digest, 16)
        sk = keypair[0]
        pk = keypair[1]

        hs = h_m - h_m2
        # L^h * r^e = L^h' * r'^e
        # (L^h * r^e)/L^h' = r'^e
        # L^(h-h')*r^e=r'^e
        # j^d^(h-h')*r=r'
        b = pow(j, sk[2], pk[1])
        #Python cannot execute pow(a, -b, c), so we need the modular inverse of a relative to c
        if hs < 0:
            b = self.modinv(b, pk[1])
            hs = -hs
        r2 = r * pow(b, hs, pk[1])
        return r2

    def collide(self, ch, j, m2, tag2, keypair):
        """
        Finds the r for a pre calculated ch value
        """
        m2_digest = hashlib.sha1(str(tag2) + "|" + m2).hexdigest()
        h_m2 = int(m2_digest, 16)
        sk = keypair[0]
        pk = keypair[1]

        # ch = (j^h_m)*(r^e)  (mod n)
        # (j^h_m)*(r^e) = (j^h_m')*(r'^e) (mod n)
        # ch * (j^h_m')^-1 = r'^e (mod n)
        # ch^d * ((j^h_m')^-1)^d = r'
        chd = pow(ch, sk[2], pk[1]) # ch^d
        l = pow(j, h_m2, pk[1]) # b^h_m2
        inv_l = self.modinv(l, pk[1]) # b^(h_m2*-1)
        inv_l_pow_d = pow(inv_l, sk[2], pk[1])

        r2 = (chd * inv_l_pow_d) % pk[1]
        return r2

    def genkey(self):
        """
        Generates a RSA key
        """
        def is_prime(num):
            if num == 2:
                return True
            if num < 2 or num % 2 == 0:
                return False
            for n in range(3, int(num**0.5)+2, 2):
                if num % n == 0:
                    return False
            return True

        j = random.randint(10, 100)
        p = j
        while not is_prime(p):
            j = random.randint(10, 100)
            p = j
        k = random.randint(10, 100)
        q = k
        while not is_prime(q):
            k = random.randint(10, 100)
            q = k

        n = p * q
        phi = (p - 1) * (q - 1)
        e = random.randint(0, phi)
        g, x, y = self.egcd(e, phi)
        while g != 1:
            e = random.randint(1, phi)
            g, x, y = self.egcd(e, phi)

        d = self.modinv(e, phi)

        return [(p, q, d), (e, n)]

def main():
    chash = CHash()
    keypair = chash.genkey()
    #keypair = ((11, 13, 120), (7, 143))
    print("Key pair: {}".format(keypair))

    sk = keypair[0]
    pk = keypair[1]

    """
    Checks if RSA is correct
    """
    c = pow(12, pk[0], pk[1])
    m = pow(c, sk[2], pk[1])
    assert m == 12

    """
    The next step should be a secure RSA signature, it is not.
    """
    label = random.randint(1, 1000)
    tag = random.randint(1, 1000)
    r = random.randint(1, 1000)

    print("label: {}".format(label))
    print("tag: {}".format(tag))
    print("r: {}".format(r))
    m = "This is my message"
    print("CH(m, label, tag, r, pk): {}".format(chash.digest(m, label, tag, r, pk)))

    tag2 = random.randint(1, 1000)
    print("tag2: {}".format(tag2))
    m2 = "This is another message"
    print("m2: {}".format(m2))
    r2 = chash.adapt(m, label, tag, r, m2, tag2, keypair)
    print("r': {}".format(r2))
    print("CH(m2, label, tag2, r2, pk): {}".format(chash.digest(m2, label, tag2, r2, pk)))
    assert chash.digest(m2, label, tag2, r2, pk) ==\
        chash.digest(m, label, tag, r, pk)

    random_ch = random.randint(1, 1000000)
    print("random_ch: {}".format(random_ch))
    r_random = chash.collide(random_ch, label, m, tag, keypair)
    print("r_random: {}".format(r_random))
    print("CH(m, label, tag, r_random, pk): {}".format(chash.digest(m, label, tag, r_random, pk)))
    assert random_ch == chash.digest(m, label, tag, r_random, pk)

def full_test():
    ch = CHash()
    keypair = ch.genkey()
    print("keypair: {}".format(keypair))

    #label = random.randint(1, 143)
    m = "This is my message"
    tag = random.randint(1, 143)

    right_results = 0
    wrong_results = 0
    for label in range(1, keypair[1][1]):
        for i in range(1, keypair[1][1]):
            random_ch = i

            try:
                new_r = ch.collide(random_ch, label, m, tag, keypair)
                result = ch.digest(m, label, tag, new_r, keypair[1])
                #print("label = {}\t\ti = {}\t\tnew_r = {}\t\tresult = {}\t\tkeypair = {}".format(label, i, new_r, result, keypair))
                if i != result:
                    wrong_results += 1
                    #print("label = {}\t\ti = {}\t\tnew_r = {}\t\tresult = {}\t\tkeypair = {}".format(label, i, new_r, result, keypair))
                else:
                    right_results += 1
            except Exception as e:
                print("{}".format(e))
                wrong_results += 1

    print("Wrong results: {}\t\t{}%\t\t(Total: {})".format(wrong_results, wrong_results / (wrong_results + right_results), wrong_results + right_results))

if __name__ == "__main__":
    print("running main test:")
    main()
    print("\n\nRunning space search test:")
    full_test()
