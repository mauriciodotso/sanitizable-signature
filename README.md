# README #

A simple implementation of a sanitizable signature scheme. Not for practical use, a great deal of the operations are insecure because no proper encoding or secure random number generators were used.

## Talk with me ##

mauricio.so 'at' posgrad.ufsc.br