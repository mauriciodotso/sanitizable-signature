import chameleon_hash
import random
import hashlib

class SanSig(object):
    """
    An implementation of the scheme with the transparency property proposed on:
    de Meer, Hermann, et al. "Scope of security properties of sanitizable signatures revisited."
    Availability, Reliability and Security (ARES), 2013 Eighth International Conference on. IEEE, 2013.
    """

    def __init__(self, ch):
        self._ch = ch

    def sign(self, m, signer_keypair, sanitizer_pk, grp, adm):
        signer_sk = signer_keypair[0]
        k = signer_sk[3]
        signer_pk = signer_keypair[1]

        assert len(grp) == len(adm)
        assert signer_sk is not None
        assert sanitizer_pk is not None
        assert len(m) > 0

        #print("m: {}".format(m))
        l = len(m)
        gamma = len(grp)
        nounces = [random.randint(0, 10000) for n in range(0, gamma)]
        #print("Nounces: {}".format(nounces))
        rs = [random.randint(0, 10000) for n in range(0, gamma)]
        #print("rs: {}".format(rs))

        """
        The chameleon hash scheme requires a label parameter, to
        fix this I'm defining the tag as the tuple (i, j) where
        the first is the label and the second is the tag as defined in
        the paper. This should be verified to guarantee if we are still
        compliant with the scheme proposed.
        """
        tags = []
        for n in range(0, gamma):
            rand_inst = random.WichmannHill(k)
            rand_inst.jumpahead(nounces[n])
            x = rand_inst.randint(0, 10000)
            #print("x: {}".format(x))
            prg = random.WichmannHill(x)
            tags.append((random.randint(0, 10000), prg.randint(0, 10000)))
        #print("tags: {}".format(tags))

        hs = [None]
        for i in range(1, gamma):
            blocks = [m[n] for n in grp[i]]
            # print("blocks: {}".format(blocks))
            blocksstr = ""
            for block in blocks:
                blocksstr += block
            msg = str(i) + "|" +  blocksstr
            ch = self._ch.digest(msg, tags[i][0], tags[i][1], rs[i], sanitizer_pk)
            hs.append(ch)

        m0 = ""
        for i in range(1, gamma):
            m0 += str(tags[i]) + "|"

        for i in range(0, l):
            m0 += m[i] + "|"

        m0 += str(signer_pk)
        #print("m0: {}".format(m0))
        hs[0] = self._ch.digest(m0, tags[0][0], tags[0][1], rs[0], sanitizer_pk)
        #print("hs: {}".format(hs))

        msg = str(hs) + "|" + str(grp[0]) + "|" + str(sanitizer_pk) + "|" + str(adm) + "|" + str(grp)
        #print("msg: {}".format(msg))
        msg_digest = int(hashlib.sha1(msg).hexdigest(), 16)
        #print("msg_digest: {}".format(msg_digest))
        sigma_c = pow(msg_digest, signer_sk[2], signer_pk[1])
        #print("sigma_c = pow(msg_digest, signer_sk[2], signer_pk[1]): {} = pow({}, {}, {})".format(sigma_c, msg_digest, signer_sk[2], signer_pk[1]))
        return (sigma_c, tags, nounces, adm, grp, rs)

    def verify(self, m, s, signer_pk, sanitizer_pk):
        sigma_c, tags, nounces, adm, grps, rs = s

        l = len(m)
        gamma = len(grps)
        hs = [None]
        for i in range(1, gamma):
            grp = grps[i]
            blocks = [m[n] for n in grp]
            blockstr = ""
            for block in blocks:
                blockstr += block
            msg = str(i) + "|" + blockstr
            #print("msg: {}".format(msg))
            ch = self._ch.digest(msg, tags[i][0], tags[i][1], rs[i], sanitizer_pk)
            hs.append(ch)

        m0 = ""
        for i in range(1, gamma):
            m0 += str(tags[i]) + "|"

        for i in range(0, l):
            m0 += m[i] + "|"
        m0 += str(signer_pk)
        hs[0] = self._ch.digest(m0, tags[0][0], tags[0][1], rs[0], sanitizer_pk)
        #print("hs: {}".format(hs))

        msg = str(hs) + "|" + str(grps[0]) + "|" + str(sanitizer_pk) + "|" + str(adm) + "|" + str(grps)
        #print("msg: {}".format(msg))
        msg_digest = int(hashlib.sha1(msg).hexdigest(), 16)
        #print("sigma_c: {}".format(sigma_c))
        verify_sigma_c = pow(sigma_c, signer_pk[0], signer_pk[1])
        #print("msg_digest  (mod signer_pk[1]): {}".format(msg_digest % signer_pk[1]))
        #print("verify_sigma_c: {}".format(verify_sigma_c))
        return msg_digest % signer_pk[1] == verify_sigma_c

    def sanitize(self, m, mod, s, signer_pk, sanitizer_keypair):
        # The signature should be valid for the sanitization
        assert self.verify(m, s, signer_pk, sanitizer_keypair[1]) == True

        sigma_c, tags, nounces, adm, grps, rs = s

        new_m = m[:] #Clone the list to not modify m
        """
        The MOD set and ADM set are inconsistent, thus I'm modifing it
        so the implementation becomes easy here
        """
        for i in mod.keys():
            if not adm[i]:
                raise Exception("Change not admissible.")
            for l in mod[i]:
                new_m[l] = mod[i][l]

        new_nounces = nounces[:] # Clone
        new_tags = tags[:] # Clone
        for i in mod.keys():
            new_nounces[i] = random.randint(0, 10000)
            new_tags[i] = (new_tags[i][0], random.randint(0, 10000))

        if len(mod) > 0:
            new_nounces[0] = random.randint(0, 10000)
            new_tags[0] = (tags[0][0], random.randint(0, 10000))

        new_rs = rs[:] #Clone
        for i in mod.keys():
            msg = "".join([m[n] for n in grps[i]])
            msg = str(i) + "|" + msg

            new_msg = "".join([new_m[n] for n in grps[i]])
            new_msg = str(i) + "|" + new_msg
            #                            m,  label,       tag,        r,      m2,       tag2,          keypair
            new_rs[i] = self._ch.adapt(msg, tags[i][0], tags[i][1], rs[i], new_msg, new_tags[i][1], sanitizer_keypair)

        m0 = "|".join([str(t) for t in tags[1:]]) + "|" + "|".join(m) + "|" + str(signer_pk)
        #print("m0: {}".format(m0))
        new_m0 = "|".join([str(t) for t in new_tags[1:]]) + "|" + "|".join(new_m) + "|" + str(signer_pk)
        #print("new_m0: {}".format(new_m0))
        new_rs[0] = self._ch.adapt(m0, tags[0][0], tags[0][1], rs[0], new_m0, new_tags[0][1], sanitizer_keypair)

        return new_m, (sigma_c, new_tags, new_nounces, adm, grps, new_rs)


    def proof(self, signer_keypair, m, s, other_m, other_s, sanitizer_pk):
        signer_pk = signer_keypair[1]
        signer_sk = signer_keypair[0]
        sigma_c, tags, nounces, adm, grps, rs = s
        other_sigma_c, other_tags, other_nounces, other_adm, other_grps,\
            other_rs = other_s

        assert sigma_c == other_sigma_c

        gamma = len(grps)
        for i in range(1, gamma):
            grp = grps[i]
            blocks = [m[n] for n in grp]
            blocksstr = str(i) + "|" + "".join(blocks)
            #print("blockstr: {}".format(blocksstr))

            other_grp = other_grps[i]
            other_blocks = [other_m[n] for n in other_grp]
            other_blocksstr = str(i) + "|" + "".join(other_blocks)
            #print("other_blocksstr: {}".format(other_blocksstr))

            ch = self._ch.digest(blocksstr, tags[i][0], tags[i][1], rs[i], sanitizer_pk)
            #print("ch: {}".format(ch))
            other_ch = self._ch.digest(other_blocksstr, other_tags[i][0], other_tags[i][1], other_rs[i], sanitizer_pk)
            #print("other_ch: {}".format(other_ch))
            assert ch == other_ch

        m0 = "|".join([str(t) for t in tags[1:]]) + "|" + "|".join(m) + "|" + str(signer_pk)
        other_m0 = "|".join([str(t) for t in other_tags[1:]]) + "|" + "|".join(other_m) + "|" + str(signer_pk)
        assert self._ch.digest(m0, tags[0][0], tags[0][1], rs[0], sanitizer_pk) == \
            self._ch.digest(other_m0, other_tags[0][0], other_tags[0][1], other_rs[0], sanitizer_pk)

        xs = []
        k = signer_sk[3]
        proof_tags = tags[:]
        for n in range(gamma):
            rand_inst = random.WichmannHill(k)
            rand_inst.jumpahead(nounces[n])
            x = rand_inst.randint(0, 10000)
            xs.append(x)
            prg = random.WichmannHill(x)
            proof_tags[n] = ((proof_tags[n][0], prg.randint(0, 10000)))

        return (tags, m, signer_pk, sanitizer_pk, rs, xs)

    def judge(self, m, s, signer_pk, sanitizer_pk, proof):
        sigma_c, tags, nounces, adm, grps, rs = s
        proof_tags, proof_m, proof_signer_pk, proof_sanitizer_pk, proof_rs, proof_xs = proof

        assert self.verify(m, s, signer_pk, sanitizer_pk) == True
        assert sanitizer_pk == proof_sanitizer_pk
        gamma = len(grps)
        d = []
        for i in range(gamma):
            prg = random.WichmannHill(proof_xs[i])
            if prg.randint(0, 10000) == proof_tags[i][1]:
                d.append('SIG')
            else: #Non trivial collision
                d.append('SAN')

        return d


def main():
    m = ["This is a secret document.", "This document will be sanitized eventually.",  "The sanitization should work as expected."]
    chash = chameleon_hash.CHash()
    signer_keypair = chash.genkey()
    signer_keypair[0] = (signer_keypair[0][0], signer_keypair[0][1], signer_keypair[0][2], random.randint(0, 100000), ) # adds the k to the secret key
    print("Signer((p, q, d, k), (e, n)): \n\t{}\n\n".format(signer_keypair))
    sanitizer_keypair = chash.genkey()
    print("Sanitizer((p, q, d), (e, n)): \n\t{}\n\n".format(sanitizer_keypair))
    sanSig = SanSig(chash)
    print("="*80)
    grp = [[], [0], [1], [2]]
    adm = [False, True, True, True]
    sigma = sanSig.sign(m, signer_keypair, sanitizer_keypair[1], grp, adm)
    print("sigma (sigma_c, tags, nounces, adm, grp, rs): \n\t{}\n\n".format(sigma))
    print("="*80)
    validation = sanSig.verify(m, sigma, signer_keypair[1], sanitizer_keypair[1])
    print("\t\tValidation: {}\n\n".format(validation))

    """
    MOD: Grp[i] -> Block[i] -> m'
    """
    mod = {
        #1: {0: "This is a ###### document."},
        2: {1: "This document was sanitized."},
        3: {2: "The sanitization worked as expected."}
    }

    signer_pk = signer_keypair[1]
    new_m, sanitized_sigma = sanSig.sanitize(m, mod, sigma, signer_pk, sanitizer_keypair)
    print("="*80)
    print("new_m: {}".format(new_m))
    print("="*80)
    print("sanitized sigma (sigma_c, tags, nounces, adm, grp, rs): \n\t{}".format(sanitized_sigma))
    new_validation = sanSig.verify(new_m, sanitized_sigma, signer_keypair[1], sanitizer_keypair[1])
    print("="*80)
    print("\t\tnew validation: {}\n\n".format(new_validation))

    proof = sanSig.proof(signer_keypair, new_m, sanitized_sigma, m, sigma, sanitizer_keypair[1])
    print("="*80)
    print("proof (TAGs, m, signer_pk, sanitizer_pk, rs, xs): \n\t{}\n\n".format(proof))

    judge = sanSig.judge(new_m, sanitized_sigma, signer_keypair[1], sanitizer_keypair[1], proof)
    print("="*80)
    print("judge: {}".format(judge))

    other_proof = sanSig.proof(signer_keypair, m, sigma, m, sigma, sanitizer_keypair[1])
    print("="*80)
    print("other proof (TAGs, m, signer_pk, sanitizer_pk, rs, xs): \n\t{}\n\n".format(other_proof))
    other_judge = sanSig.judge(m, sigma, signer_keypair[1], sanitizer_keypair[1], other_proof)
    print("="*80)
    print("other judge: {}".format(other_judge))

if __name__ == "__main__":
    main()
